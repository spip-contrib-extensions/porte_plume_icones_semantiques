<?php
// Icones sémantiques pour le Porte Plume
// par Bertrand Marne
//
//remplacer les raccourcis gras/italiques par emphase (em) et emphase forte (strong)
//
$debut_italique = '<em>' ;
$fin_italique = '</em>'  ;
$debut_gras = '<strong>' ;
$fin_gras = '</strong>' ;
?>